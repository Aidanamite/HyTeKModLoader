﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

#if GAME_IS_RAFT
using Text = TMPro.TextMeshProUGUI;
#endif

namespace HMLLibrary
{
    public class ModManagerPage : MenuPage
    {
        public static ModManagerPage instance;
        public static List<ModData> modList = new List<ModData>();
        public static List<Mod> activeModInstances = new List<Mod>();
        public static List<string> openedModpacks = new List<string>();
        public static Dictionary<string, Assembly> loadedAssemblies = new Dictionary<string, Assembly>();
        public static Transform ModListContent;
        public static GameObject ModEntryPrefab;
        public static GameObject ModpackEntryPrefab;
        public static GameObject ModpackSubEntryPrefab;
        public static Button LoadModsBtn;
        public static Button UnloadModsBtn;
        public static Button ModListRefreshBtn;
        public static GameObject ModsGameObjectParent;
        public static bool bypassValueChangeCheckAll;
        public static GameObject modInfoObj;
        public static GameObject noModsText;
        public static Sprite modpackIcon;
        public static Sprite modpackBanner;

#if GAME_IS_RAFT
        public static Color greenColor = new Color(136f / 255, 216f / 255, 176f / 255);
        public static Color orangeColor = new Color(255f / 255, 204f / 255, 92f / 255);
        public static Color redColor = new Color(255f / 255, 111f / 255, 105f / 255);
        public static Color blueColor = new Color(150f / 255, 185f / 255, 226f / 255);
        public static Color yellowColor = new Color(255f / 255, 238f / 255, 173f / 255);
#else
        public static Color greenColor = new Color(67.0f / 255, 181.0f / 255, 129.0f / 255);
        public static Color orangeColor = new Color(181.0f / 255, 135.0f / 255, 67.0f / 255);
        public static Color redColor = new Color(181.0f / 255, 67.0f / 255, 67.0f / 255);
        public static Color blueColor = new Color(67.0f / 255, 129.0f / 255, 181.0f / 255);
        public static Color yellowColor = new Color(232.0f / 255, 226.0f / 255, 58.0f / 255);
#endif


        public static bool canRefreshModlist = true;

        static int listRefreshAmount = 0;
        public static PermanentModsList UserPermanentMods;
        public static PermanentModsList OnStartUserPermanentMods;

        public override async void Initialize()
        {
            instance = this;
            ModsGameObjectParent = new GameObject();
            ModsGameObjectParent.name = "ModsPrefabs";
            DontDestroyOnLoad(ModsGameObjectParent);
            modInfoObj = transform.Find("ModScrollView").Find("Viewport").Find("ModInfo").gameObject;
            modInfoObj.SetActive(false);
            noModsText = transform.Find("ModScrollView").Find("Viewport").Find("HML_FindMods").gameObject;
            noModsText.GetComponent<Button>().onClick.AddListener(() => Application.OpenURL(AssemblyLoader.Loader.website + "mods"));
            noModsText.SetActive(false);
            ModEntryPrefab = await HLib.bundle.TaskLoadAssetAsync<GameObject>("ModEntry");
            ModpackEntryPrefab = await HLib.bundle.TaskLoadAssetAsync<GameObject>("ModpackEntry");
            ModpackSubEntryPrefab = await HLib.bundle.TaskLoadAssetAsync<GameObject>("ModpackSubEntry");
            ModListContent = transform.Find("ModScrollView").Find("Viewport").Find("HMLModManager_ModListContent");
            modpackIcon = await HLib.bundle.TaskLoadAssetAsync<Sprite>("icon_modpack");
            modpackBanner = await HLib.bundle.TaskLoadAssetAsync<Sprite>("banner_modpack");
            transform.Find("InfoBar").Find("HML_CheckAllToggle").GetComponent<Toggle>().onValueChanged.AddListener(var => SelectAllMods(var));
            LoadModsBtn = transform.Find("HML_LoadModsBtn").GetComponent<Button>();
            LoadModsBtn.onClick.AddListener(LoadSelectedMods);
            UnloadModsBtn = transform.Find("HML_UnloadModsBtn").GetComponent<Button>();
            UnloadModsBtn.onClick.AddListener(UnloadSelectedMods);
            ModListRefreshBtn = transform.Find("Buttons").Find("HML_RefreshModsBtn").GetComponent<Button>();
            ModListRefreshBtn.onClick.AddListener(() => RefreshMods());
            transform.Find("Buttons").Find("HML_OpenModsFolderBtn").GetComponent<Button>().onClick.AddListener(OpenModsFolder);
            RefreshMods();
        }

        public static void OpenModsFolder()
        {
            Process.Start("explorer.exe", Path.GetFullPath(HLib.path_modsFolder));
        }

        public static void RefreshModsStates()
        {
            modList.ForEach(m => RefreshModState(m));
        }

        public static void RefreshModState(ModData md)
        {
            if (md.modinfo.ModlistEntry == null) { return; }
            if (md.modinfo.modState == ModInfo.ModStateEnum.running)
            {
                if (md.modinfo.goInstance != null && md.modinfo.mainClass != null)
                {
                    md.modinfo.mainClass.modlistEntry = md;
                }
                if (md.modinfo.modpackData == null)
                {
                    if (!md.jsonmodinfo.isModPermanent)
                    {
                        md.modinfo.permanentModWarning?.SetActive(false);
                        md.modinfo.unloadBtn?.SetActive(true);
                        md.modinfo.loadBtn?.SetActive(false);
                        if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()) && md.modinfo.modpackData == null)
                        {
                            md.modinfo.permanentModWarning.SetActive(true);
#if GAME_IS_GREENHELL
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
#else
                            md.modinfo.permanentModWarning.transform.GetComponent<Image>().sprite = HLib.bundle.LoadAsset<Sprite>("info_bg");
#endif
                            md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("Text").GetComponent<Text>().text = "Loaded at startup";
                        }
                    }
                    else
                    {
                        md.modinfo.unloadBtn?.SetActive(false);
                        md.modinfo.loadBtn?.SetActive(false);
                        md.modinfo.permanentModWarning?.SetActive(true);
                    }
                }
            }
            else
            {
                if (md.modinfo.modpackData == null)
                {
                    if (!md.jsonmodinfo.isModPermanent)
                    {
                        md.modinfo.permanentModWarning.SetActive(false);
                        md.modinfo.unloadBtn.SetActive(false);
                        md.modinfo.loadBtn.SetActive(true);
                        if (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))
                        {
                            md.modinfo.permanentModWarning.SetActive(true);
#if GAME_IS_GREENHELL
                        ColorBlock block = md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors;
                        block.disabledColor = new Color(84.0f / 255.0f, 166.0f / 255.0f, 87.0f / 255.0f, 200.0f / 255.0f);
                        md.modinfo.permanentModWarning.transform.GetComponent<Button>().colors = block;
#else
                            md.modinfo.permanentModWarning.transform.GetComponent<Image>().sprite = HLib.bundle.LoadAsset<Sprite>("info_bg");
#endif
                            md.modinfo.permanentModWarning.transform.Find("Tooltip").Find("Text").GetComponent<Text>().text = "Loaded at startup";
                        }
                    }
                    else
                    {
                        md.modinfo.unloadBtn?.SetActive(false);
                        md.modinfo.loadBtn?.SetActive(false);
                        if (md.modinfo.modState != ModInfo.ModStateEnum.needrestart)
                            md.modinfo.permanentModWarning.SetActive(true);
                    }
                }
            }
            UnityEngine.UI.Text Statustext = md.modinfo.ModlistEntry.transform.Find("ModStatusText").GetComponent<UnityEngine.UI.Text>();
            switch (md.modinfo.modState)
            {
                case ModInfo.ModStateEnum.running:
                    Statustext.text = "Running...";
                    Statustext.color = greenColor;
                    break;
                case ModInfo.ModStateEnum.errored:
                    Statustext.text = "Errored";
                    Statustext.color = redColor;
                    break;
                case ModInfo.ModStateEnum.idle:
                    Statustext.text = "Not loaded";
                    Statustext.color = blueColor;
                    break;
                case ModInfo.ModStateEnum.compiling:
                    Statustext.text = "Compiling...";
                    Statustext.color = yellowColor;
                    break;
                case ModInfo.ModStateEnum.needrestart:
                    Statustext.text = "Needs Game Restart";
                    Statustext.color = redColor;
                    break;
            }
            if (md.modinfo.modpackData != null)
            {
                Statustext.text += md.modinfo.modpackData?.jsonData?.name;
            }
        }

        public async static void LoadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.isModPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.LoadMod(md);
                }
                await Task.Delay(1);
            }
        }

        public async static void UnloadSelectedMods()
        {
            foreach (ModData md in modList.ToArray())
            {
                if (md.jsonmodinfo.isModPermanent) { continue; }
                if (md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn)
                {
                    md.modinfo.modHandler.UnloadMod(md);
                }
                await Task.Delay(1);
            }
        }

        public static void SelectAllMods(bool var)
        {
            if (!bypassValueChangeCheckAll)
            {
                foreach (ModData md in modList.ToArray())
                {
                    md.modinfo.ModlistEntry.GetComponentInChildren<Toggle>().isOn = var;
                }
            }
            else
            {
                bypassValueChangeCheckAll = false;
            }
        }

        public static async void RefreshMods()
        {
            if (!canRefreshModlist) { return; }
            listRefreshAmount++;
            canRefreshModlist = false;
            noModsText.SetActive(true);
            foreach (Transform t in ModsGameObjectParent.transform)
            {
                bool found = false;
                foreach (ModData moddata in modList.ToArray())
                {
                    if (moddata.modinfo.modFile.Name.ToLower() == t.gameObject.name)
                    {
                        found = true;
                    }
                }
                if (!found)
                {
                    Destroy(t.gameObject);
                }
            }
            modList.Clear();
            LoadModsBtn.interactable = false;
            UnloadModsBtn.interactable = false;
            ModListRefreshBtn.interactable = false;
            foreach (Transform t in ModListContent.transform)
            {
                Destroy(t.gameObject);
            }
            DirectoryInfo d = new DirectoryInfo(Path.Combine(Application.dataPath, "..\\mods"));
            //List<FileInfo> mods = d.GetFiles("*", SearchOption.AllDirectories).ToList();
            List<FileInfo> mods = d.GetFiles("*", SearchOption.TopDirectoryOnly).ToList();
            foreach (FileInfo file in mods)
            {
                await RefreshMod(file);
                await Task.Delay(1);
            }
            await Task.Delay(1);
            InitializeModpacks();
            LoadModsBtn.interactable = true;
            UnloadModsBtn.interactable = true;
            ModListRefreshBtn.interactable = true;
            RefreshModsStates();
            canRefreshModlist = true;
        }

        public static void InitializeModpacks()
        {
            List<ModpackData> permanentModpacks = new List<ModpackData>();
            List<ModpackData> nonPermanentModpacks = new List<ModpackData>();
            foreach (ModData x in modList.ToArray())
            {
                if (x.modinfo.modpackData == null) { continue; }
                if (x.jsonmodinfo.isModPermanent)
                {
                    permanentModpacks.Add(x.modinfo.modpackData);
                }
            }

            permanentModpacks.ForEach(x =>
            {
                if (x.mods.Where(a => a.modinfo.modState == ModInfo.ModStateEnum.running || a.modinfo.modState == ModInfo.ModStateEnum.compiling).Any())
                {
                    x.permanentModWarning?.SetActive(true);
                    x.loadBtn?.SetActive(false);
                    x.unloadBtn?.SetActive(false);
                }
                else
                {

                    x.permanentModWarning?.SetActive(true);
                    x.loadBtn?.SetActive(true);
                    x.unloadBtn?.SetActive(false);
                }

                x.isPermanent = true;
            });

            LoadModpacks();
        }

        public static List<ModpackData> GetModpacks()
        {
            return modList.Where(x => x.modinfo.modpackData != null).Select(x => x.modinfo.modpackData).Distinct().ToList();
        }

        public static void LoadModpacks()
        {
            FindObjectsOfType<ModpackData>().ToList().ForEach(x =>
            {
                if (openedModpacks.Contains(x.folderName))
                {
                    x.GetComponent<ModpackToggleHandler>()?.ForceOpen();
                }
            });

            List<ModpackData> list = GetModpacks();
            list.ForEach(md =>
            {
                if (!UserPermanentMods.list.Contains("modpack." + md.folderName) && listRefreshAmount == 1)
                {
                    md.LoadModpack();
                    return;
                }
                if (!md.isPermanent)
                {
                    bool modsLoaded = md.mods.Where(x => x.modinfo.modState == ModInfo.ModStateEnum.running || x.modinfo.modState == ModInfo.ModStateEnum.compiling).Any();
                    if (md.loadBtn && md.unloadBtn)
                    {
                        md.loadBtn.SetActive(!modsLoaded);
                        md.unloadBtn.SetActive(modsLoaded);
                    }
                }
            });
        }
        public static List<ModData> TestList = new List<ModData>();
        public static async Task RefreshMod(FileInfo file)
        {
            try
            {
                ModData _md = modList.Where(m => m.modinfo.modFile == file).FirstOrDefault();
                if (_md != null && _md.modinfo.ModlistEntry != null)
                {
                    Destroy(_md.modinfo.ModlistEntry);
                    modList.Remove(_md);
                }
                ModData md = null;
                string filename = file.Name.ToLower();
                //string folderName = Path.GetFileName(Path.GetDirectoryName(file.FullName));
                // This should temporarily disable modpacks until i fix the shit.
                string folderName = "mods";
                if (!folderName.StartsWith("modpack.") && folderName != "mods") { return; }
                if (folderName != "mods" && Path.GetFileName(Path.GetDirectoryName(Path.GetDirectoryName(file.FullName))) != "mods")
                {
                    Debug.LogWarning("Mod " + file.Name + " is not inside mods folder or inside modpack folder !");
                    return;
                }
                if (filename.EndsWith(AssemblyLoader.Loader.modFormat))
                {
                    md = await new ZipModHandler().GetModData(file);
                }
                else if (filename.EndsWith(".lnk"))
                {
                    md = await new FolderModHandler().GetModData(file);
                }
                else
                {
                    return;
                }

                if (md == null)
                {
                    Debug.LogError("[ModManager] " + file.Name + " > Couldn't load the mod! Invalid ModData!");
                    return;
                }
                md.registrationTick = DateTime.Now.Ticks;
                if (md.jsonmodinfo == null)
                {
                    Debug.LogError("[ModManager] " + file.Name + " > The mod is missing a modinfo.json file!");
                    return;
                }
                if (folderName == "mods")
                {
                    md.modinfo.ModlistEntry = Instantiate(ModEntryPrefab, ModListContent.transform.position, ModListContent.transform.rotation, ModListContent.transform);
                    md.modinfo.ModlistEntry.GetComponent<RectTransform>().sizeDelta = new Vector2(580, 25);
                    md.modinfo.ModlistEntry.GetComponent<RectTransform>().ForceUpdateRectTransforms();
                }
                else
                {
                    Transform modpackEntryParent = ModListContent.Find("modpack." + folderName);

                    ModpackData d = modpackEntryParent?.GetComponent<ModpackData>();
                    if (modpackEntryParent == null)
                    {
                        GameObject modpackGameobject = Instantiate(ModpackEntryPrefab, ModListContent.transform.position, ModListContent.transform.rotation, ModListContent.transform);
                        d = modpackGameobject.AddComponent<ModpackData>();
                        d.folderName = folderName;
                        modpackGameobject.name = "modpack." + folderName;
                        modpackEntryParent = modpackGameobject.transform;
                        modpackGameobject.GetComponent<RectTransform>().sizeDelta = new Vector2(580, 25);
                        modpackGameobject.GetComponent<RectTransform>().ForceUpdateRectTransforms();
                        ModpackToggleHandler m = modpackGameobject.AddComponent<ModpackToggleHandler>();
                        m.folderName = d.folderName;

                        d.infoBtn = modpackGameobject.transform.Find("Buttons").Find("ModInfoBtn").gameObject;
                        d.infoBtnTooltip = modpackGameobject.transform.Find("Buttons").Find("ModInfoBtn").Find("Tooltip").gameObject;
                        d.infoBtn.AddComponent<TooltipHandler>().tooltip = d.infoBtnTooltip;

                        d.infoBtn.GetComponent<Button>().onClick.AddListener(() =>
                        {
                            ShowModpackInfo(d);
                        });

                        d.permanentModWarning = modpackGameobject.transform.Find("Buttons").transform.Find("PermanentModWarning").gameObject;
                        d.permanentTooltip = modpackGameobject.transform.Find("Buttons").transform.Find("PermanentModWarning").Find("Tooltip").gameObject;
                        d.permanentModWarning.AddComponent<TooltipHandler>().tooltip = d.permanentTooltip;

                        d.loadBtn = modpackGameobject.transform.Find("Buttons").Find("LoadModBtn").gameObject;
                        d.loadBtnTooltip = modpackGameobject.transform.Find("Buttons").Find("LoadModBtn").Find("Tooltip").gameObject;
                        d.loadBtn.AddComponent<TooltipHandler>().tooltip = d.loadBtnTooltip;

                        d.unloadBtn = modpackGameobject.transform.Find("Buttons").Find("UnloadModBtn").gameObject;
                        d.unloadBtnTooltip = modpackGameobject.transform.Find("Buttons").Find("UnloadModBtn").Find("Tooltip").gameObject;
                        d.unloadBtn.AddComponent<TooltipHandler>().tooltip = d.unloadBtnTooltip;

                        d.permanentModWarning?.SetActive(false);
                        d.permanentTooltip?.SetActive(false);
                        d.loadBtn?.SetActive(false);
                        d.loadBtnTooltip?.SetActive(false);
                        d.unloadBtn?.SetActive(false);
                        d.unloadBtnTooltip?.SetActive(false);

                        d.unloadBtn.GetComponent<Button>().onClick.AddListener(() => d.UnloadModpack());
                        d.loadBtn.GetComponent<Button>().onClick.AddListener(() => d.LoadModpack());
                        string modpackInfoFile = Path.Combine(Path.GetDirectoryName(file.FullName), "modpackinfo.json");
                        if (File.Exists(modpackInfoFile))
                        {
                            try
                            {
                                d.jsonData = JsonConvert.DeserializeObject<ModpackDataJson>(File.ReadAllText(modpackInfoFile));
                                await Task.Delay(1);
                                modpackGameobject.transform.Find("ModName").GetComponent<UnityEngine.UI.Text>().text = HUtils.StripRichText(d.jsonData.name);
                                modpackGameobject.transform.Find("ModAuthor").GetComponent<UnityEngine.UI.Text>().text = HUtils.StripRichText(d.jsonData.author);
                            }
                            catch
                            {
                                Debug.LogError("[ModManager] " + d.folderName + " > Couldn't read the modpackinfo.json file!\nYou should consider reinstalling the pack!");
                            }
                        }
                        else
                        {
                            Debug.LogError("[ModManager] " + d.folderName + " > Couldn't find the modpackinfo.json file!\nYou should consider reinstalling the pack!");
                        }
                    }
                    md.modinfo.modpackData = d;
                    md.modinfo.ModlistEntry = Instantiate(ModpackSubEntryPrefab, ModListContent.transform.position, ModListContent.transform.rotation, modpackEntryParent.transform.Find("Modlist"));
                    md.modinfo.ModlistEntry.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -15f + 30 - (modpackEntryParent.transform.Find("Modlist").childCount * 30));
                    d.mods.Add(md);
                }
                if (ModsGameObjectParent.transform.Find(md.modinfo.modFile.Name.ToLower()))
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.running;
                }

                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<UnityEngine.UI.Text>().text = (md.modinfo.isShortcut ? "{DEV} " : "") + HUtils.StripRichText(md.jsonmodinfo.name);
#if GAME_IS_RAFT
                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<UnityEngine.UI.Text>().color = md.modinfo.isShortcut ? new Color(40f / 255f, 199f / 255f, 69f / 255f) : new Color(187f / 255f, 161f / 255f, 106f / 255f);
#else
                md.modinfo.ModlistEntry.transform.Find("ModName").GetComponent<UnityEngine.UI.Text>().color = md.modinfo.isShortcut ? new Color(40f / 255f, 199f / 255f, 69f / 255f) : Color.white;
#endif
                md.modinfo.ModlistEntry.transform.Find("ModAuthor").GetComponent<UnityEngine.UI.Text>().text = HUtils.StripRichText(md.jsonmodinfo.author);
                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<UnityEngine.UI.Text>().text = HUtils.StripRichText(md.jsonmodinfo.version);
                md.modinfo.buttons = md.modinfo.ModlistEntry.transform.Find("Buttons").gameObject;
                md.modinfo.infoBtn = md.modinfo.buttons.transform.Find("ModInfoBtn").gameObject;
                md.modinfo.infoBtnTooltip = md.modinfo.buttons.transform.Find("ModInfoBtn").Find("Tooltip").gameObject;
                md.modinfo.infoBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.infoBtnTooltip;
                if (md.modinfo.modpackData == null)
                {
                    md.modinfo.permanentModWarning = md.modinfo.buttons.transform.Find("PermanentModWarning").gameObject;
                    md.modinfo.permanentTooltip = md.modinfo.buttons.transform.Find("PermanentModWarning").Find("Tooltip").gameObject;
                    md.modinfo.permanentModWarning.AddComponent<TooltipHandler>().tooltip = md.modinfo.permanentTooltip;
                    md.modinfo.loadBtn = md.modinfo.buttons.transform.Find("LoadModBtn").gameObject;
                    md.modinfo.loadBtnTooltip = md.modinfo.buttons.transform.Find("LoadModBtn").Find("Tooltip").gameObject;
                    md.modinfo.loadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.loadBtnTooltip;
                    md.modinfo.unloadBtn = md.modinfo.buttons.transform.Find("UnloadModBtn").gameObject;
                    md.modinfo.unloadBtnTooltip = md.modinfo.buttons.transform.Find("UnloadModBtn").Find("Tooltip").gameObject;
                    md.modinfo.unloadBtn.AddComponent<TooltipHandler>().tooltip = md.modinfo.unloadBtnTooltip;
                    md.modinfo.permanentModWarning?.SetActive(false);
                    md.modinfo.loadBtn?.SetActive(false);
                    md.modinfo.unloadBtn?.SetActive(false);

                }
                md.modinfo.versionTooltip = md.modinfo.ModlistEntry.transform.Find("ModVersionText").Find("VersionTooltip").gameObject;
                md.modinfo.ModlistEntry.transform.Find("ModVersionText").gameObject.AddComponent<TooltipHandler>().tooltip = md.modinfo.versionTooltip;
                if (md.modinfo.modFiles.ContainsKey(md.jsonmodinfo.icon))
                {
                    Texture2D potentialIcon = new Texture2D(2, 2);
                    potentialIcon.LoadImage(md.modinfo.modFiles[md.jsonmodinfo.icon]);
                    md.modinfo.ModIcon = potentialIcon;
                    md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = potentialIcon;
                }
                else
                {
                    md.modinfo.ModIcon = HLib.missingTexture;
                    md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = HLib.missingTexture;
                }
#if GAME_IS_RAFT
                if (md.jsonmodinfo.author == "Update Me!")
                {
                    md.modinfo.ModIcon = HLib.bundle.LoadAsset<Texture2D>("deprecatedMod");
                    md.modinfo.ModlistEntry.transform.Find("ModIconMask").Find("ModIcon").GetComponent<RawImage>().texture = HLib.bundle.LoadAsset<Texture2D>("deprecatedMod");
                }
#endif
                if (md.modinfo.modFiles.ContainsKey(md.jsonmodinfo.banner))
                {
                    Texture2D potentialBanner = new Texture2D(2, 2);
                    potentialBanner.LoadImage(md.modinfo.modFiles[md.jsonmodinfo.banner]);
                    md.modinfo.ModBanner = potentialBanner;
                }
                else
                {
                    md.modinfo.ModBanner = HLib.missingTexture;
                }
                HUtils.GetModVersion(md.jsonmodinfo.updateUrl).ContinueWith((t) =>
                {
                    UnityMainThreadDispatcher.Instance().Enqueue(async () =>
                    {
                        try
                        {
                            if (t.Result == "Unknown" || t.Result == "")
                            {
                                md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "Unknown";
                                md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<UnityEngine.UI.Text>().color = orangeColor;
                            }
                            else
                            {
                                if (t.Result != md.jsonmodinfo.version)
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "New version available!";
                                    if (md.modinfo.modpackData == null)
                                    {
                                        Debug.LogWarning("[ModManager] " + md.jsonmodinfo.name + " > The current installed version is outdated! A new version is available! (" + t.Result + ")");
                                        HNotify.Get().AddNotification(HNotify.NotificationType.scaling, md.jsonmodinfo.name + " has a new version available!", 5, await HLib.bundle.TaskLoadAssetAsync<Sprite>("DownloadIcon"));
                                    }
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<UnityEngine.UI.Text>().color = redColor;
                                }
                                else
                                {
                                    md.modinfo.versionTooltip.GetComponentInChildren<Text>().text = "Up to date!";
                                    md.modinfo.ModlistEntry.transform.Find("ModVersionText").GetComponent<UnityEngine.UI.Text>().color = greenColor;
                                }
                            }
                        }
                        catch { }
                    });
                });
                if (md.modinfo.modpackData == null)
                {
                    md.modinfo.unloadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.UnloadMod(md));
                    md.modinfo.loadBtn.GetComponent<Button>().onClick.AddListener(() => md.modinfo.modHandler.LoadMod(md));
                }
                md.modinfo.infoBtn.GetComponent<Button>().onClick.AddListener(() =>
                {
                    ShowModInfo(md);
                });

                if (md.modinfo.modpackData == null && (md.jsonmodinfo.isModPermanent || (OnStartUserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower()))) && listRefreshAmount == 1)
                {
                    md.modinfo.modHandler.LoadMod(md);
                }
                else if (md.jsonmodinfo.isModPermanent && listRefreshAmount > 1 && md.modinfo.modState == ModInfo.ModStateEnum.idle && md.modinfo.modpackData == null)
                {
                    md.modinfo.modState = ModInfo.ModStateEnum.needrestart;
                }

                RefreshModState(md);
                modList.Add(md);
                TestList.Add(md);
                noModsText.SetActive(false);
            }
            catch (Exception e)
            {
                Debug.LogError("[ModManager] " + file.Name + " > A fatal error occured while loading the mod!\nStacktrace : " + e.ToString());
            }
            await Task.Delay(1);
        }

        public static void ShowModpackInfo(ModpackData md)
        {
            modInfoObj.transform.Find("ModName").GetComponent<Text>().text = md.jsonData.name;
            modInfoObj.transform.Find("Author").GetComponent<Text>().text = "Author : " + md.jsonData.author;
            modInfoObj.transform.Find("ModVersion").GetComponent<Text>().text = "Mods Count : " + md.mods.Count;
            modInfoObj.transform.Find("GameVersion").GetComponent<Text>().text = "Is Permanent : " + (md.isPermanent ? "Yes" : "No");
            modInfoObj.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = modpackBanner.texture;
            modInfoObj.transform.Find("BannerMask").Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = modpackIcon.texture;
            modInfoObj.transform.Find("Description").GetComponent<Text>().text = "Description : \n\n" + md.jsonData.description;
            modInfoObj.transform.Find("MakePermanent").gameObject.SetActive(true);
            modInfoObj.transform.Find("MakePermanent").GetComponentInChildren<Text>().text = "LOAD THIS MODPACK AT STARTUP";
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().isOn = !UserPermanentMods.list.Contains("modpack." + md.folderName);
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.AddListener((val) =>
            {
                SetModPermanent(!val, "modpack." + md.folderName);
            });
            modInfoObj.SetActive(true);
        }

        public static void ShowModInfo(ModData md)
        {
            modInfoObj.transform.Find("ModName").GetComponent<Text>().text = md.jsonmodinfo.name;
            modInfoObj.transform.Find("Author").GetComponent<Text>().text = "Author : " + md.jsonmodinfo.author;
            modInfoObj.transform.Find("ModVersion").GetComponent<Text>().text = "Version : " + md.jsonmodinfo.version;
            modInfoObj.transform.Find("GameVersion").GetComponent<Text>().text = "Game Version : " + md.jsonmodinfo.gameVersion;
            modInfoObj.transform.Find("BannerMask").Find("Banner").GetComponent<RawImage>().texture = md.modinfo.ModBanner;
            modInfoObj.transform.Find("BannerMask").Find("IconMask").Find("Icon").GetComponent<RawImage>().texture = md.modinfo.ModIcon;
            modInfoObj.transform.Find("Description").GetComponent<Text>().text = "Description : \n\n" + md.jsonmodinfo.description;
            
            modInfoObj.transform.Find("MakePermanent").gameObject.SetActive(!md.jsonmodinfo.isModPermanent && md.modinfo.modpackData == null);
            modInfoObj.transform.Find("MakePermanent").GetComponentInChildren<Text>().text = "LOAD THIS MOD AT STARTUP";
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.RemoveAllListeners();
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().isOn = UserPermanentMods.list.Contains(md.modinfo.modFile.Name.ToLower());
            modInfoObj.transform.Find("MakePermanent").GetComponent<Toggle>().onValueChanged.AddListener((val) =>
            {
                SetModPermanent(val, md.modinfo.modFile.Name.ToLower());
            });
            modInfoObj.SetActive(true);
        }

        public static void SetModPermanent(bool val, string fileName)
        {
            if (val)
            {
                if (!UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Add(fileName);
                }
            }
            else
            {
                if (UserPermanentMods.list.Contains(fileName))
                {
                    UserPermanentMods.list.Remove(fileName);
                }
            }

            PlayerPrefs.SetString(AssemblyLoader.Loader.settingsPrefix + "UserPermanentMods", JsonUtility.ToJson(UserPermanentMods));
        }
    }

    [Serializable]
    public class ModpackDataJson
    {
        public string name = "Modpack Example Name";
        public string author = "Unknown";
        public string description = "...";
    }

    public class ModpackData : MonoBehaviour
    {
        public string folderName = "mods";
        public ModpackDataJson jsonData = new ModpackDataJson();
        public bool isPermanent = false;
        public GameObject buttons;
        public GameObject infoBtn;
        public GameObject loadBtn;
        public GameObject unloadBtn;
        public GameObject permanentModWarning;
        public GameObject permanentTooltip;
        public GameObject infoBtnTooltip;
        public GameObject loadBtnTooltip;
        public GameObject unloadBtnTooltip;
        public List<ModData> mods = new List<ModData>();

        public void LoadModpack()
        {
            if (!isPermanent)
            {
                if (loadBtn && unloadBtn)
                {
                    loadBtn.SetActive(false);
                    unloadBtn.SetActive(true);
                }
            }
            else
            {
                if (SceneManager.GetActiveScene().name != "MainMenuScene")
                {
                    HNotify.Get().AddNotification(HNotify.NotificationType.scaling, "You can only load permanent modpacks in the main menu !", 10, HNotify.ErrorSprite);
                    return;
                }
                if (loadBtn && unloadBtn && permanentModWarning)
                {
                    loadBtn.SetActive(false);
                    unloadBtn.SetActive(false);
                    permanentModWarning.SetActive(true);
                }
            }
            foreach (ModData m in mods.Where(x => x.modinfo.modState != ModInfo.ModStateEnum.running && x.modinfo.modState != ModInfo.ModStateEnum.compiling).ToList())
            {
                m.modinfo.modHandler.LoadMod(m);
            }
        }

        public void UnloadModpack()
        {
            if (!isPermanent)
            {
                loadBtn.SetActive(true);
                unloadBtn.SetActive(false);
            }
            List<ModData> mods = ModManagerPage.modList.Where(x => x.modinfo.modpackData.folderName == folderName).ToList();
            foreach (ModData m in mods)
            {
                m.modinfo.modHandler.UnloadMod(m);
            }
        }
    }
}
