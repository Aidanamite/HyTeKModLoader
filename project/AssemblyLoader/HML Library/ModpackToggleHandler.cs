﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HMLLibrary
{
    public class ModpackToggleHandler : MonoBehaviour, IPointerDownHandler
    {
        public bool state = false;
        public string folderName;
        public RectTransform rect;
        public Transform modlist;
        public Transform icon;
        public bool initialized;

        public void Start()
        {
            rect = GetComponent<RectTransform>();
            modlist = transform.Find("Modlist");
            icon = transform.Find("Arrow");
            initialized = true;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            StopAllCoroutines();
            StartCoroutine(Toggle());
        }

        public async void ForceOpen()
        {
            while (!initialized) { await Task.Delay(1); }
            state = false;
            StopAllCoroutines();
            StartCoroutine(Toggle());
        }

        public IEnumerator Toggle(float speed = 1)
        {
            while (!initialized) { yield return new WaitForSeconds(0.01f); }
            if (modlist != null && rect != null && icon != null)
            {
                if (speed < 1)
                {
                    state = false;
                }
                state = !state;
                if (state)
                {
                    ModManagerPage.openedModpacks?.AddUniqueOnly(folderName);
                }
                else
                {
                    ModManagerPage.openedModpacks?.Remove(folderName);
                }
                float finalSize = state ? (25 + (modlist.childCount * 30) + 7.5f) : 25;
                float finalRot = state ? 0 : 90;
                while (rect.sizeDelta.y != finalSize || icon.transform.rotation.eulerAngles.z != finalRot)
                {
                    float velocity = 0;
                    icon.transform.rotation = Quaternion.Euler(0, 0, Mathf.SmoothDamp(icon.transform.rotation.eulerAngles.z, finalRot, ref velocity, speed * Time.deltaTime));
                    rect.sizeDelta = new Vector2(580, Mathf.SmoothDamp(rect.sizeDelta.y, finalSize, ref velocity, speed * Time.deltaTime));
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }
}