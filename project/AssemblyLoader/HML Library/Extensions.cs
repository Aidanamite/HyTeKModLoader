﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using System.Reflection;
using System.Globalization;
using Object = UnityEngine.Object;

public static class Extensions
{
#if GAME_IS_RAFT
    public static List<NetworkChannel> TryCastObjectToChannel(this object channel, bool logErrors = true)
    {
        var fetchedChannels = new List<NetworkChannel>();
        if (channel is string)
            try
            {
                fetchedChannels.Add((NetworkChannel)Enum.Parse(typeof(NetworkChannel), channel as string));
            }
            catch (Exception e)
            {
                if (logErrors)
                    Debug.LogWarning($"[ModUtils]: Could not parse the channel value as type {typeof(NetworkChannel).FullName}\n{e}");
            }
        else if (channel is IEnumerable<string>)
            foreach (string value in channel as IEnumerable<string>)
                try
                {
                    fetchedChannels.Add((NetworkChannel)Enum.Parse(typeof(NetworkChannel), value));
                }
                catch (Exception e)
                {
                    if (logErrors)
                        Debug.LogWarning($"[ModUtils]: Could not parse one of the channel values ({value}) as type {typeof(NetworkChannel).FullName}\n{e}");
                }
        else if (channel is IConvertible)
            try
            {
                fetchedChannels.Add((NetworkChannel)(long)(channel as IConvertible).ToType(typeof(long), System.Globalization.NumberFormatInfo.CurrentInfo));
            }
            catch (Exception e)
            {
                if (logErrors)
                    Debug.LogWarning($"[ModUtils]: Could not parse the channel value as type {typeof(NetworkChannel).FullName}\n{e}");
            }
        else
        {
            Type enumerableType = channel.GetType().GetInterfaces().FirstOrDefault(x => x.IsConstructedGenericType && x.FullName.StartsWith("System.Collections.Generic.IEnumerable"));
            if (enumerableType != null && typeof(IConvertible).IsAssignableFrom(enumerableType.GenericTypeArguments[0]))
                foreach (object value in channel as IEnumerable)
                    try
                    {
                        fetchedChannels.Add((NetworkChannel)(long)(value as IConvertible).ToType(typeof(long), System.Globalization.NumberFormatInfo.CurrentInfo));
                    }
                    catch (Exception e)
                    {
                        if (logErrors)
                            Debug.LogWarning($"[ModUtils]: Could not parse one of the channel values ({value}) as type {typeof(NetworkChannel).FullName}\n{e}");
                    }
            else if (logErrors)
                Debug.LogWarning($"[ModUtils]: Type {channel.GetType().FullName} cannot be parsed as type {typeof(NetworkChannel).FullName} or {typeof(IEnumerable<NetworkChannel>).FullName}");
        }
        return fetchedChannels;
    }
    public static Item_Base Clone(this Item_Base source, int uniqueIndex, string uniqueName)
    {
        Item_Base item = ScriptableObject.CreateInstance<Item_Base>();
        item.Initialize(uniqueIndex, uniqueName, source.MaxUses);
        item.settings_buildable = source.settings_buildable.Clone();
        item.settings_consumeable = source.settings_consumeable.Clone();
        item.settings_cookable = source.settings_cookable.Clone();
        item.settings_equipment = source.settings_equipment.Clone();
        item.settings_Inventory = source.settings_Inventory.Clone();
        item.settings_recipe = source.settings_recipe.Clone();
        item.settings_usable = source.settings_usable.Clone();
        return item;
    }
#endif
    public static List<Y> Cast<X, Y>(this IEnumerable<X> originalValues, Func<X, Y> customCast)
    {
        var newValues = new List<Y>();
        foreach (X i in originalValues)
            newValues.Add(customCast(i));
        return newValues;
    }

    static Transform _prefabHolder;
    static Transform prefabHolder
    {
        get
        {
            if (!_prefabHolder)
            {
                _prefabHolder = new GameObject("prefabHolder").transform;
                Object.DontDestroyOnLoad(_prefabHolder);
                _prefabHolder.gameObject.SetActive(false);
            }
            return _prefabHolder;
        }
    }
    public static T CreatePrefabCopy<T>(this T obj) where T : Object => Object.Instantiate(obj, prefabHolder);
    public static T CreatePrefabCopy<T>(this T obj, bool worldPositionStays) where T : Object => Object.Instantiate(obj, prefabHolder, worldPositionStays);
    public static T InstantiateInactive<T>(this T obj) where T : Object
    {
        T newObject = Object.Instantiate(obj, prefabHolder);
        newObject.GameObject()?.SetActive(false);
        newObject.Transform()?.SetParent(null);
        return newObject;
    }
    public static T InstantiateInactive<T>(this T obj, bool worldPositionStays) where T : Object
    {
        T newObject = Object.Instantiate(obj, prefabHolder, worldPositionStays);
        newObject.GameObject()?.SetActive(false);
        newObject.Transform()?.SetParent(null, worldPositionStays);
        return newObject;
    }
    static Transform Transform(this Object obj) => (obj as GameObject)?.transform ?? (obj as Component)?.transform;
    static GameObject GameObject(this Object obj) => (obj as GameObject) ?? (obj as Component)?.gameObject;

    public static void CopyFieldsOf(this object value, object source)
    {
        Type type = value.GetType();
        Type sourceType = source.GetType();
        while (!type.IsAssignableFrom(sourceType))
            type = type.BaseType;
        while (type != typeof(Object) && type != typeof(object))
        {
            foreach (FieldInfo field in type.GetFields(~BindingFlags.Default))
                if (!field.IsStatic)
                    field.SetValue(value, field.GetValue(source));
            type = type.BaseType;
        }
    }

    public static void ReplaceReferences<X>(this Component value, X original, object replacement, int serializableLayers = 0, Func<FieldInfo, X> onSetFailReplacement = null)
    {
        foreach (Component component in value.GetComponentsInChildren<Component>())
            (component as object).ReplaceReferences(original, replacement, serializableLayers, onSetFailReplacement);
    }
    public static void ReplaceReferences<X>(this GameObject value,X original, object replacement, int serializableLayers = 0, Func<FieldInfo, X> onSetFailReplacement = null)
    {
        foreach (Component component in value.GetComponentsInChildren<Component>())
            (component as object).ReplaceReferences(original, replacement, serializableLayers, onSetFailReplacement);
    }

    static bool IsEqualTo(this object a, object b) => b == a || (b?.Equals(a) ?? false);
    public static void ReplaceReferences<X>(this object value, X original, object replacement, int serializableLayers = 0, Func<FieldInfo, X> onSetFailReplacement = null)
    {
        if (value == null)
            return;
        Type searchingType = value.GetType();
        while (searchingType != typeof(Object) && searchingType != typeof(object))
        {
            foreach (FieldInfo field in searchingType.GetFields(~BindingFlags.Default))
                if (!field.IsStatic)
                {
                    if (field.GetValue(value).IsEqualTo(original))
                        try
                        {
                            field.SetValue(value, replacement);
                        }
                        catch
                        {
                            try
                            {
                                if (onSetFailReplacement != null)
                                    field.SetValue(value, onSetFailReplacement(field));
                            }
                            catch { }
                        }
                    else if (field.GetValue(value) is IList)
                    {
                        var list = field.GetValue(value) as IList;
                        for (int i = 0; i < list.Count; i++)
                            if (list[i].IsEqualTo(original))
                                try
                                {
                                    list[i] = replacement;
                                }
                                catch
                                {
                                    try
                                    {
                                        if (onSetFailReplacement != null)
                                            list[i] = onSetFailReplacement(field);
                                    }
                                    catch { }
                                }
                            else if (serializableLayers > 0 && (list[i]?.GetType()?.IsSerializable ?? false))
                                list[i].ReplaceReferences(original, replacement, serializableLayers - 1, onSetFailReplacement);
                    }
                    else if (field.GetValue(value) is IDictionary)
                    {
                        var dictionary = field.GetValue(value) as IDictionary;
                        foreach (object key in dictionary.Keys.Cast<object>())
                        { 
                            if (key.IsEqualTo(original)) { 
                                try
                                {
                                    dictionary[replacement] = dictionary[key];
                                    if (dictionary[replacement].IsEqualTo(original))
                                    {
                                        try
                                        {
                                            dictionary[replacement] = replacement;
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                if (onSetFailReplacement != null)
                                                    dictionary[replacement] = onSetFailReplacement(field);
                                            }
                                            catch { }
                                        }
                                    }
                                    else if (serializableLayers > 0 && (dictionary[key]?.GetType()?.IsSerializable ?? false))
                                        dictionary[key].ReplaceReferences(original, replacement, serializableLayers - 1, onSetFailReplacement);
                                }
                                catch { }
                                dictionary.Remove(key);
                            }
                            else
                            {
                                if (serializableLayers > 0 && (key?.GetType()?.IsSerializable ?? false))
                                    key.ReplaceReferences(original, replacement, serializableLayers - 1, onSetFailReplacement);
                                if (dictionary[key].IsEqualTo(original))
                                {
                                    try
                                    {
                                        dictionary[key] = replacement;
                                    }
                                    catch
                                    {
                                        try
                                        {
                                            if (onSetFailReplacement != null)
                                                dictionary[key] = onSetFailReplacement(field);
                                        }
                                        catch { }
                                    }
                                }
                                else if (serializableLayers > 0 && (dictionary[key]?.GetType()?.IsSerializable ?? false))
                                    dictionary[key].ReplaceReferences(original, replacement, serializableLayers - 1, onSetFailReplacement);
                            }
                        }
                    }
                    else if (serializableLayers > 0 && (field.GetValue(value)?.GetType()?.IsSerializable ?? false))
                        field.GetValue(value).ReplaceReferences(original, replacement, serializableLayers - 1, onSetFailReplacement);
                }
            searchingType = searchingType.BaseType;
        }
    }

    public static Sprite ToSprite(this Texture2D texture, Rect? rect = null, Vector2? pivot = null) => Sprite.Create(texture, rect ?? new Rect(0, 0, texture.width, texture.height), pivot ?? new Vector2(0.5f, 0.5f));

    public static Texture2D GetReadable(this Texture2D source, Rect? copyArea = null, RenderTextureFormat format = RenderTextureFormat.Default, RenderTextureReadWrite readWrite = RenderTextureReadWrite.Default, TextureFormat? targetFormat = null, bool mipChain = true)
    {
        RenderTexture temp = RenderTexture.GetTemporary(source.width, source.height, 0, format, readWrite);
        Graphics.Blit(source, temp);
        temp.filterMode = FilterMode.Point;
        RenderTexture previousTexture = RenderTexture.active;
        RenderTexture.active = temp;
        Rect area = copyArea ?? new Rect(0, 0, temp.width, temp.height);
        area.y = temp.height - area.y - area.height;
        Texture2D texture = new Texture2D((int)area.width, (int)area.height, targetFormat ?? TextureFormat.RGBA32, mipChain);
        texture.ReadPixels(area, 0, 0);
        texture.Apply();
        RenderTexture.active = previousTexture;
        RenderTexture.ReleaseTemporary(temp);
        return texture;
    }

    public static Texture2D CloneWithoutMipmap(this Texture2D source, bool makeNotReadable = false)
    {
        var newTexture = new Texture2D(source.width, source.width, source.format, false);
        newTexture.SetPixels(source.GetPixels(0));
        newTexture.Apply(true, makeNotReadable);
        return newTexture;
    }

    public static float ToFloatInvariant(this string value)
    {
        if (string.IsNullOrWhiteSpace(value))
            return 1;
        if (value.Contains(",") && !value.Contains("."))
            value = value.Replace(',', '.');
        CultureInfo prevCulture = CultureInfo.CurrentCulture;
        CultureInfo.CurrentCulture = CultureInfo.GetCultureInfoByIetfLanguageTag("en-NZ");
        Exception exception = null;
        float result = 0;
        try
        {
            result = float.Parse(value);
        }
        catch (Exception e)
        {
            exception = e;
        }
        CultureInfo.CurrentCulture = prevCulture;
        if (exception != null)
            throw exception;
        return result;
    }
    public static double ToDoubleInvariant(this string value)
    {
        if (string.IsNullOrWhiteSpace(value))
            return 1;
        if (value.Contains(",") && !value.Contains("."))
            value = value.Replace(',', '.');
        CultureInfo prevCulture = CultureInfo.CurrentCulture;
        CultureInfo.CurrentCulture = CultureInfo.GetCultureInfoByIetfLanguageTag("en-NZ");
        Exception exception = null;
        double result = 0;
        try
        {
            result = double.Parse(value);
        }
        catch (Exception e)
        {
            exception = e;
        }
        CultureInfo.CurrentCulture = prevCulture;
        if (exception != null)
            throw exception;
        return result;
    }
    public static Color Overlay(this Color background, Color foreground)
    {
        if (background.a <= 0)
            return foreground;
        if (foreground.a <= 0)
            return background;
        float alphaRatio = foreground.a / (foreground.a + background.a * (1 - foreground.a));
        float Ratio(float backgroundValue, float foregroundValue) => foregroundValue * alphaRatio + backgroundValue * (1 - alphaRatio);
        return new Color(Ratio(background.r, foreground.r), Ratio(background.g, foreground.g), Ratio(background.b, foreground.b), foreground.a + background.a * (1 - foreground.a));
    }
}