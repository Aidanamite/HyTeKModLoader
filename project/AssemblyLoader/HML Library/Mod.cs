﻿using UnityEngine;
using HMLLibrary;
using System.Linq;
using System;
using System.Reflection;
using System.Collections.Generic;
using Steamworks;
using Object = UnityEngine.Object;
#if GAME_IS_RAFT
using Steamworks;
#endif

public class Mod : MonoBehaviour
{
    public ModData modlistEntry;
    public static Mod modInstance
    {
        get
        {
            Assembly calling = Assembly.GetCallingAssembly();
            foreach (var mod in ModManagerPage.activeModInstances)
                if (mod && mod.GetType().Assembly == calling)
                    return mod;
            return null;
        }
        set { }
    }
    public Dictionary<string, MethodInfo> methods = new Dictionary<string, MethodInfo>();
    public CustomSerializationHandler serializer = new CustomSerializationHandler();
    public List<Object> CreatedObjects = new List<Object>();

    public Mod()
    {
        /*List<MethodInfo> m = Assembly.GetCallingAssembly().GetTypes().SelectMany(x => x.GetMethods((BindingFlags)62)).ToList();
        m.ForEach(x =>
        {
            ModMethodAttribute attr = (ModMethodAttribute)x.GetCustomAttribute(typeof(ModMethodAttribute));
            if (attr != null)
            {
                string name = string.IsNullOrWhiteSpace(attr.Name) ? x.Name : attr.Name;
                methods.Add(name, x);
                Debug.Log(Call(name));
            }
        });*/
    }

    void OnDestroy()
    {
        foreach (var obj in CreatedObjects)
            if (obj)
            {
                if (obj is AssetBundle)
                    (obj as AssetBundle).Unload(false);
                else if (obj is Transform)
                    Destroy((obj as Transform).gameObject);
                else
                    Destroy(obj);
            }
    }

    /*public object Call(string name, params object[] args)
    {
        if (methods.ContainsKey(name))
        {
            MethodInfo m = methods[name];
            Debug.Log(m);
        }
        return null;
    }

    public T Call<T>(string name, params object[] args)
    {
        return (T)((object)Convert.ChangeType(Call(name, args), typeof(T)));
    }*/

    public virtual void OnOtherModLoaded(Mod mod) { }
    public virtual void OnOtherModUnloaded(Mod mod) { }

    public virtual bool CanUnload(ref string message) => true;
    public virtual bool CanLoad(ref string message) => true;

    public virtual void UnloadMod()
    {
        modlistEntry.modinfo.modHandler.UnloadMod(modlistEntry);
    }

    public virtual byte[] GetEmbeddedFileBytes(string path)
    {
        byte[] value = null;
        modlistEntry.modinfo.modFiles.TryGetValue(path, out value);
        if (value == null)
            Debug.LogError("[ModManager] " + modlistEntry.jsonmodinfo.name + " > The file " + path + " doesn't exist!");
        return value;
    }
    public Texture2D LoadEmbeddedImage(string path, bool leaveReadable = false)
    {
        var texture = new Texture2D(0, 0);
        var fileBytes = GetEmbeddedFileBytes(path);
        if (fileBytes != null)
        {
            texture.LoadImage(fileBytes, !leaveReadable);
            if (leaveReadable)
                texture.Apply();
        }
        return texture;
    }

    public JsonModInfo GetModInfo() => modlistEntry.jsonmodinfo;

    public void Log(object message)
    {
        Debug.Log("[" + GetModInfo().name + "] : " + message.ToString());
    }
    public void LogError(object message)
    {
        Debug.LogError("[" + GetModInfo().name + "] : " + message.ToString());
    }
    public void LogWarning(object message)
    {
        Debug.LogWarning("[" + GetModInfo().name + "] : " + message.ToString());
    }

#if GAME_IS_RAFT

    public virtual RGD LocalSave
    {
        get => null;
        set { }
    }
    public virtual Message RemoteSave
    {
        get => null;
        set { }
    }

    internal readonly List<NetworkChannel> channels = new List<NetworkChannel>();
    internal static NetworkChannel[] allChannels = new NetworkChannel[0];
    public static NetworkChannel[] AllListeningChannels => allChannels;
    public virtual bool OnMessageRecieved(CSteamID steamID, NetworkChannel channel, Message message) => true;

    public bool TryProcessMessage(CSteamID steamID, NetworkChannel channel, Message message)
    {
        if (channels != null && channels.Contains(channel))
            try
            {
                return OnMessageRecieved(steamID, channel, message);
            } catch (Exception e)
            {
                LogError(e);
            }
        return false;
    }

    public class BuildMenuItem
    {
        public Item_Base itemToInsert;
        public Item_Base itemToPlaceNextTo;
        public bool addHorizontally;
        public BuildMenuItem(Item_Base ItemToInsert, Item_Base ItemToPlaceNextTo, bool AddHorizontally = false)
        {
            itemToInsert = ItemToInsert;
            itemToPlaceNextTo = ItemToPlaceNextTo;
            addHorizontally = AddHorizontally;
        }
    }

    public virtual IEnumerable<BuildMenuItem> GetBuildMenuItems() => null;

    public void StartListeningToChannel(NetworkChannel channel)
    {
        if (channel == NetworkChannel.Channel_Session || channel == NetworkChannel.Channel_Game)
        {
            Debug.LogError("Mod.StartListeningToChannel() can't be used to listen for messages on the channel 0 and 1! Please choose a unique number for your mod.");
            return;
        }
        channels.AddUniqueOnly(channel);
        var channelList = allChannels.ToList();
        channelList.AddUniqueOnly(channel);
        allChannels = channelList.ToArray();
    }

    public void StartListeningToChannels(IEnumerable<NetworkChannel> channels)
    {
        if (channels.Any(x => x == NetworkChannel.Channel_Session || x == NetworkChannel.Channel_Game))
        {
            Debug.LogError("Mod.StartListeningToChannels() can't be used to listen for messages on the channel 0 and 1! Please choose a unique number for your mod.");
            return;
        }
        this.channels.AddRangeUniqueOnly(channels);
        var channelList = allChannels.ToList();
        channelList.AddRangeUniqueOnly(channels);
        allChannels = channelList.ToArray();
    }

    public void StopListeningToChannel(NetworkChannel channel)
    {
        channels.Remove(channel);
        RecheckChannel(channel);
    }

    public void StopListeningToChannels(IEnumerable<NetworkChannel> channels)
    {
        this.channels.RemoveAll(x => channels.Contains(x));
        RecheckChannels(channels);
    }

    internal static void RecheckChannel(NetworkChannel channel)
    {
        foreach (var m in ModManagerPage.activeModInstances)
            if (m.channels.Contains(channel))
                return;
        var channelList = allChannels.ToList();
        channelList.Remove(channel);
        allChannels = channelList.ToArray();
    }
    internal static void RecheckChannels(IEnumerable<NetworkChannel> channels)
    {
        var checkChannels = channels.ToList();
        foreach (var mod in ModManagerPage.activeModInstances)
            foreach (var channel in mod.channels)
                checkChannels.Remove(channel);
        var channelList = allChannels.ToList();
        foreach (var channel in checkChannels)
            channelList.Remove(channel);
        allChannels = channelList.ToArray();
    }



    public virtual void WorldEvent_WorldLoaded()
    {
    }

    public virtual void WorldEvent_WorldSaved()
    {
    }

    public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)
    {
    }

    public virtual void LocalPlayerEvent_Death(Vector3 deathPosition)
    {
    }

    public virtual void LocalPlayerEvent_Respawn()
    {
    }

    public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item)
    {
    }

    public virtual void LocalPlayerEvent_PickupItem(PickupItem item)
    {
    }

    public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
    {
    }

    public virtual void LocalPlayerEvent_OnRespawn()
    {
    }

    public virtual void WorldEvent_OnPlayerConnected(CSteamID steamid, RGD_Settings_Character characterSettings)
    {
    }

    public virtual void WorldEvent_OnPlayerDisconnected(CSteamID steamid, DisconnectReason disconnectReason)
    {
    }

    public virtual void WorldEvent_WorldUnloaded()
    {
    }

    public string slug
    {
        get
        {
            return GetModInfo().slug;
        }
    }

    public string oldSlug
    {
        get
        {
            return GetModInfo().oldSlug;
        }
    }
#endif

    // JsonModInfo Exposed Fields
    public new string name
    {
        get
        {
            return GetModInfo().name;
        }
    }
    public string author
    {
        get
        {
            return GetModInfo().author;
        }
    }
    public string description
    {
        get
        {
            return GetModInfo().description;
        }
    }
    public string version
    {
        get
        {
            return GetModInfo().version;
        }
    }
    public string license
    {
        get
        {
            return GetModInfo().license;
        }
    }
    public string icon
    {
        get
        {
            return GetModInfo().icon;
        }
    }
    public string banner
    {
        get
        {
            return GetModInfo().banner;
        }
    }
    public string gameVersion
    {
        get
        {
            return GetModInfo().gameVersion;
        }
    }
    public string updateUrl
    {
        get
        {
            return GetModInfo().updateUrl;
        }
    }
    public bool isModPermanent
    {
        get
        {
            return GetModInfo().isModPermanent;
        }
    }
    public bool requiredByAllPlayers
    {
        get
        {
            return GetModInfo().requiredByAllPlayers;
        }
    }
    public List<string> excludedFiles
    {
        get
        {
            return GetModInfo().excludedFiles;
        }
    }
}