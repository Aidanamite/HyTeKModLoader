﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using HarmonyLib;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Remoting.Messaging;
using System.Runtime.InteropServices;


public class CustomSerializationHandler
{
    static Dictionary<Type, Serializer> serializers = new Dictionary<Type, Serializer>()
    {
        [typeof(string)] = new Serializer(x => Encoding.Unicode.GetBytes((string)x), x => Encoding.Unicode.GetString(x))
    };

    public Dictionary<Type, Serializer> customSerializers = new Dictionary<Type, Serializer>();

    internal CustomSerializationHandler() { }

    public byte[] Serialize(params object[] values)
    {
        var objectBytes = new List<byte>();
        var bytes = new List<byte>();
        var stringSerializer = GetSerializer(typeof(string));
        foreach (object value in values)
        {
            if (value == null)
                bytes.AddRange(stringSerializer.ToBytes("null"));
            else
            {
                var objectSerializer = GetSerializer(value.GetType());
                if (objectSerializer == null)
                    throw new FormatException("Could not find suitable serializer for type " + value.GetType().FullName + ". To register a new serializer, implement and use the ModUtils_RegisterSerializer method");
                objectBytes.AddRange(objectSerializer.ToBytes(value));
                bytes.AddRange(stringSerializer.ToBytes(value.GetType().FullName));
            }
        }
        bytes.InsertRange(0, GetSerializer(typeof(int)).ToBytes(values.Length));
        bytes.AddRange(objectBytes);
        if (bytes.Count % 2 == 1)
            bytes.Add(0);
        return bytes.ToArray();
    }
    public static string SerialToString(byte[] serial)
    {
        if (serial.Length % 4 != 0)
        {
            var newData = new byte[serial.Length + (4 - (serial.Length % 4))];
            serial.CopyTo(newData, 0);
            serial = newData;
        }
        return Encoding.Unicode.GetString(serial);
    }
    public static byte[] StringToSerial(string value) => Encoding.Unicode.GetBytes(value);
    public object[] Deserialize(byte[] data, bool logErrors = true)
    {
        var pos = 0;
        var count = (int)GetSerializer(typeof(int)).ToObject(data, ref pos);
        var types = new Type[count];
        var objects = new object[count];
        var stringSerializer = GetSerializer(typeof(string));
        for (int i = 0; i < count; i++)
        {
            var typeName = (string)stringSerializer.ToObject(data, ref pos);
            types[i] = AccessTools.TypeByName(typeName);
            if (typeName != "null" && types[i] == null && logErrors)
                Debug.LogError($"[ModUtils]: Message parse failed to fetch type: " + typeName + "\n" + Environment.StackTrace);
        }
        for (int i = 0; i < count; i++)
        {
            if (types[i] == null)
                continue;
            var s = GetSerializer(types[i]);
            if (s == null)
            {
                if (logErrors)
                    Debug.LogError("[ModUtils]: Could not find suitable serializer for type " + types[i].FullName + ". Message may belong to a different mod or you may be using an different version to another player\n" + Environment.StackTrace);
            }
            else
                objects[i] = s.ToObject(data, ref pos);
        }
        return objects;
    }
    Serializer GetSerializer(Type type)
    {
        foreach (KeyValuePair<Type,Serializer> serializerPairs in customSerializers)
            if (serializerPairs.Key == type)
                return serializerPairs.Value;
        foreach (KeyValuePair<Type, Serializer> serializerPairs in serializers)
            if (serializerPairs.Key == type)
                return serializerPairs.Value;
        try { return new MarshalSerializer(type); } catch { }
        var enumerableType = type.GetInterfaces().FirstOrDefault(x => x.IsConstructedGenericType && x.FullName.StartsWith("System.Collections.Generic.IEnumerable"));
        if (enumerableType != null && enumerableType.GenericTypeArguments[0] != typeof(object))
            return new CollectionSerializer(GetSerializer(enumerableType.GenericTypeArguments[0]), enumerableType.GenericTypeArguments[0]);
        if (type.GetInterfaces().Contains(typeof(IEnumerable)))
            return new ObjectCollectionSerializer(this);
        if (type.IsSerializable)
            return new SerializableSerializer();
        return null;
    }
}

public class Serializer
{
    Func<object, byte[]> converter;
    Func<byte[], object> reverter;
    public readonly int? size;
    protected Serializer() { }
    public Serializer(Func<object, byte[]> toBytes, Func<byte[], object> fromBytes)
    {
        converter = toBytes;
        reverter = fromBytes;
    }
    public Serializer(Func<object, byte[]> toBytes, Func<byte[], object> fromBytes, int bytesPerObject) : this(toBytes, fromBytes)
    {
        size = bytesPerObject;
    }
    public virtual object ToObject(byte[] source, ref int start)
    {
        int s;
        if (size == null)
        {
            s = BitConverter.ToInt32(source, start);
            start += 4;
        }
        else
            s = size.Value;
        var newBytes = new byte[s];
        for (int i = 0; i < s; i++)
            newBytes[i] = source[start + i];
        start += s;
        try
        {
            return reverter(newBytes);
        }
        catch (Exception e)
        {
            throw new SerializationException("An error occured while deserializing the data",e);
        }
    }
    public virtual byte[] ToBytes(object source)
    {
        byte[] bytes;
        try
        {
            bytes = converter(source);
        }
        catch (Exception e)
        {
            throw new SerializationException("An error occured while serializing the data", e);
        }
        if (size == null)
        {
            var b = new byte[bytes.Length + 4];
            BitConverter.GetBytes(bytes.Length).CopyTo(b,0);
            bytes.CopyTo(b, 4);
            return b;
        }
        else
        {
            if (bytes.Length != size.Value)
                Array.Resize(ref bytes, size.Value);
            return bytes;
        }
    }
}

internal class CollectionSerializer : Serializer
{
    Serializer BaseSerializer;
    Type BaseType;
    public CollectionSerializer(Serializer baseSerializer, Type baseType)
    {
        BaseSerializer = baseSerializer;
        BaseType = baseType;
    }
    public override byte[] ToBytes(object source)
    {
        var bytes = new List<byte>();
        var itemCount = 0;
        foreach (var o in (IEnumerable)source)
        {
            bytes.AddRange(BaseSerializer.ToBytes(o));
            itemCount++;
        }
        bytes.InsertRange(0, BitConverter.GetBytes(itemCount));
        return bytes.ToArray();
    }
    public override object ToObject(byte[] source, ref int start)
    {
        var itemCount = BitConverter.ToInt32(source, start);
        var objects = BaseType.MakeArrayType().GetConstructor(new[] { typeof(int) }).Invoke(new object[] { itemCount }) as IList;
        start += 4;
        for (var i = 0; i < itemCount; i++)
            objects[i] = BaseSerializer.ToObject(source, ref start);
        return objects;
    }
}

internal class ObjectCollectionSerializer : Serializer
{
    CustomSerializationHandler handler;
    public ObjectCollectionSerializer(CustomSerializationHandler Handler)
    {
        handler = Handler;
    }
    public override byte[] ToBytes(object source)
    {
        var objects = new List<object>();
        foreach (var o in (IEnumerable)source)
            objects.Add(o);
        var bytes = handler.Serialize(objects.ToArray()).ToList();
        bytes.InsertRange(0, BitConverter.GetBytes(bytes.Count));
        return bytes.ToArray();
    }
    public override object ToObject(byte[] source, ref int start)
    {
        var itemCount = BitConverter.ToInt32(source, start);
        var bytes = new byte[itemCount];
        start += 4;
        for (int i = 0; i < itemCount; i++)
            bytes[i] = source[i + start];
        start += itemCount;
        return handler.Deserialize(bytes);
    }
}

public class MarshalSerializer : Serializer
{
    Type objType;
    new int size;
    public MarshalSerializer(Type objectType)
    {
        size = Marshal.SizeOf(objType);
        objType = objectType;
    }
    public override byte[] ToBytes(object source)
    {
        var bytes = new byte[size];
        var ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(source, ptr, false);
        Marshal.Copy(ptr, bytes, 0, size);
        Marshal.FreeHGlobal(ptr);
        return bytes;
    }
    public override object ToObject(byte[] source, ref int start)
    {
        var bytes = new byte[size];
        var ptr = Marshal.AllocHGlobal(size);
        Marshal.Copy(bytes, start, ptr, size);
        start += size;
        var obj = Marshal.PtrToStructure(ptr, objType);
        Marshal.FreeHGlobal(ptr);
        return obj;
    }
}

public class SerializableSerializer : Serializer
{
    public override byte[] ToBytes(object source)
    {
        var stream = new MemoryStream();
        RAPI.BinaryFormatter.Serialize(stream, source);
        return stream.ToArray();
    }
    public override object ToObject(byte[] source, ref int start)
    {
        var stream = new MemoryStream(source);
        stream.Position = start;
        var obj = RAPI.BinaryFormatter.Deserialize(stream);
        start = (int)stream.Position;
        return obj;
    }
}