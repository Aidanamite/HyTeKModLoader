﻿using HMLLibrary;
using IniParser;
using IniParser.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace AssemblyLoader
{
    public class Loader
    {
        static GameObject gameObject;
#if GAME_IS_GREENHELL
        public static int assembliesCount = 85;
        public static string folderName = "GreenHellModLoader";
        public static string mainClass = "GHML_Main";
        public static string website = "https://www.greenhellmodding.com/";
        public static string modFormat = ".ghmod";
        public static string settingsPrefix = "ghmlSettings_";
#elif GAME_IS_RAFT
        public static int assembliesCount = 85;
        public static string folderName = "RaftModLoader";
        public static string mainClass = "RML_Main";
        public static string website = "https://www.raftmodding.com/";
        public static string modFormat = ".rmod";
        public static string settingsPrefix = "rmlSettings_";
#endif

        public static List<string> embedded_assemblies = new List<string>()
        {
            "0Harmony.dll",
            "SharpZipLib.dll",
            "System.ValueTuple.dll",
        };

        public async static void Load()
        {
            while (AppDomain.CurrentDomain.GetAssemblies().Length < assembliesCount)
            {
                await Task.Delay(1500);
            }
            await Task.Delay(1500);

            gameObject = new GameObject("HML");
            gameObject.AddComponent<HyTeKInjector>();
            GameObject.DontDestroyOnLoad(gameObject);
        }

        private static void Application_logMessageReceived(string condition, string stackTrace, LogType type)
        {
            HyTeKInjector.ModLoaderLog(condition);
        }
    }

    public class HyTeKInjector : MonoBehaviour
    {
        [Serializable]
        public class LauncherConfiguration
        {
            public string gamePath = "";
            public string coreVersion = "?";
            public int agreeWithTOS = 0;
            public bool skipSplashScreen = false;
            public bool disableAutomaticGameStart = false;
            public string branch = "public";
        }

        public static string modloaderFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Loader.folderName);
        public static string modloader_configfile = Path.Combine(modloaderFolder, "config.ini");
        public static LauncherConfiguration modloader_config = null;
        public static Process csc;

        public static LauncherConfiguration GetLauncherConfiguration()
        {
            LauncherConfiguration config = new LauncherConfiguration();
            var parser = new FileIniDataParser();

            IniData data = parser.ReadFile(modloader_configfile);
            config.gamePath = data["launcher"]["gamePath"];
            config.coreVersion = data["launcher"]["coreVersion"];
            config.agreeWithTOS = int.Parse(data["launcher"]["agreeWithTOS"]);
            config.skipSplashScreen = bool.Parse(data["launcher"]["skipSplashScreen"]);
            config.disableAutomaticGameStart = bool.Parse(data["launcher"]["disableAutomaticGameStart"]);
            config.branch = data["launcher"]["branch"];

            return config;
        }

        void Awake()
        {
            Directory.CreateDirectory(modloaderFolder);
            Directory.CreateDirectory(Path.Combine(modloaderFolder, "logs"));

            try
            {
                HyTeKInjector.StartCSC();
            }
            catch (Exception e) { ModLoaderLog("An exception occured when starting the mod compiler!\nException : " + e.ToString()); }


            if (File.Exists(modloader_configfile))
            {
                modloader_config = GetLauncherConfiguration();
                StartModLoader();
            }
        }

        public static void LoadEmbeddedAssemblies()
        {
            foreach (string s in Loader.embedded_assemblies)
            {
                try
                {
                    byte[] ba = GetAssemblyBytes("AssemblyLoader.Dependencies." + s, s);
                    Assembly asm = Assembly.Load(ba);
                    string path = Path.Combine(modloaderFolder, "binaries/" + s);
                    try
                    {
                        File.WriteAllBytes(path, ba);
                    }
                    catch { }
                }
                catch (Exception oof) { ModLoaderLog("An error occured when extracting an embedded assembly (" + s + ")!\n" + oof.ToString()); }
            }
        }

        void OnApplicationQuit()
        {
            Process.GetProcessesByName("csc").ToList().ForEach(p =>
            {
                p.Kill();
            });
            Process.GetCurrentProcess().Kill();
        }

        public static async Task StartCSC()
        {
            if (csc == null || csc.HasExited)
            {
                Process.GetProcessesByName("csc").ToList().ForEach(p =>
                {
                    p.Kill();
                });
                try
                {
                    byte[] ba = GetAssemblyBytes("AssemblyLoader.Dependencies.csc.exe", "csc.exe");
                    File.WriteAllBytes(Path.Combine(modloaderFolder, "csc.exe"), ba);

                    csc = new Process();
                    csc.StartInfo.UseShellExecute = false;
                    csc.StartInfo.CreateNoWindow = true;
                    csc.StartInfo.FileName = Path.Combine(HLib.path_dataFolder, "csc.exe");
                    csc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    csc.StartInfo.ErrorDialog = false;
                    csc.StartInfo.RedirectStandardInput = true;
                    csc.StartInfo.RedirectStandardOutput = true;
                    csc.EnableRaisingEvents = true;
                    bool v = csc.Start();
                    if (v)
                    {
                        ModLoaderLog("Successfully started csc.exe process!");
                    }
                    else
                    {
                        ModLoaderLog("Something is preventing csc.exe from starting!");
                    }
                    csc.BeginOutputReadLine();
                    string preloadsamplePath = Path.Combine(HLib.path_cacheFolder_temp, "csc_preloadsample.cs");
                    File.WriteAllText(preloadsamplePath, "public class PRELOAD{}");
                    string hash = HUtils.GetFileSHA512Hash(preloadsamplePath);
                    csc.StandardInput.WriteLine(string.Format("-s \"{0}\" -n \"{1}\" -o \"{2}\" -r \"{3}\" \"{4}\" -c \"{5}\"",
                        preloadsamplePath, // Input
                        "csc_preloadsample", // Assembly name
                        Path.Combine(HLib.path_cacheFolder_mods, hash + ".dll"), // Output
                        Application.dataPath + "/Managed", // Referenced assemblies
                        HLib.path_binariesFolder, // Referenced assemblies
                        hash)); // Async check string
                }
                catch (Exception e)
                {
                    ModLoaderLog("An exception occured when starting the mod compiler!\nException : " + e.ToString());
                }
            }
            await Task.Delay(5000);
            await StartCSC();
        }

        public static void StartModLoader()
        {
            try
            {
                string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
                File.WriteAllText(logfilePath, "");
                string dllFile = Path.Combine(modloaderFolder, "binaries/coremod.dll");
                if (!GameObject.Find("HyTeKModLoader") && !GameObject.Find("HyTeKDedicatedServer"))
                {
                    LoadEmbeddedAssemblies();
                    Assembly assembly = Assembly.Load(File.ReadAllBytes(dllFile));
                    ModLoaderLog("Loaded assembly : " + assembly.ToString());
                    GameObject gameObject = new GameObject("HyTeKModLoader");
                    GameObject.DontDestroyOnLoad(gameObject);
                    gameObject.AddComponent(assembly.GetType(Loader.mainClass));
                    ModLoaderLog("HyTeKModLoader has been successfully loaded!");
                }
            }
            catch (Exception e)
            {
                ModLoaderLog("/!\\ StartModLoader() : A fatal error occured! /!\\\n" + e.ToString());
            }
        }

        public static byte[] GetAssemblyBytes(string embeddedResource, string fileName)
        {
            byte[] ba = null;
            Assembly curAsm = Assembly.GetExecutingAssembly();

            using (Stream stm = curAsm.GetManifestResourceStream(embeddedResource))
            {
                if (stm == null)
                    throw new Exception(embeddedResource + " is not found in Embedded Resources.");

                ba = new byte[(int)stm.Length];
                stm.Read(ba, 0, (int)stm.Length);
                if (ba.Length > 1000)
                {
                    return ba;
                }
            }
            return null;
        }

        public static void ModLoaderLog(string text)
        {
            string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
            File.AppendAllText(logfilePath, text + "\n");
        }
    }
}