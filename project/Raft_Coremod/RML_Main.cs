﻿using HarmonyLib;
using HMLLibrary;
using RaftModLoader;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;
using Debug = UnityEngine.Debug;
using System.Linq;
using Steamworks;
using UnityEngine.SceneManagement;

public class RML_Main : MonoBehaviour
{
    private bool debug = false;

    public static List<string> logs = new List<string>();

    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;

    [Obsolete("Please use HLib.path_modsFolder instead")]
    public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");

    void OnEnable()
    {
        Application.logMessageReceived += HandleUnityLog;
    }
    void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (debug)
        {
            logs.Add(logString + "\n" + stackTrace);
        }
    }

    void OnGUI()
    {
        if (debug)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.red;
            string text = "";
            foreach (string s in logs)
            {
                text += "\n" + s;
            }
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text, style);
        }
    }

    private void OnSceneChanged(Scene scene1, Scene scene2)
    {
        Raft_Network.OnJoinResult += OnJoinResult;
    }

    public static async void OnJoinResult(CSteamID remoteID, InitiateResult result)
    {
        ServersPage.OnJoinResult(remoteID, result);
        NetworkModManager.OnJoinResult(remoteID, result);
    }

    private void Awake()
    {
        SceneManager.activeSceneChanged += OnSceneChanged;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        Directory.CreateDirectory(HLib.path_dataFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder_mods);
        Directory.CreateDirectory(HLib.path_cacheFolder_textures);
        Directory.CreateDirectory(HLib.path_cacheFolder_temp);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));

        // Free up space in cache.
        try
        {
            foreach (string f in Directory.GetFiles(HLib.path_cacheFolder_mods))
                if (!f.EndsWith("_" + Application.version + ".dll"))
                    try
                    {
                        File.Delete(f);
                    }
                    catch { }

            foreach (string f in Directory.GetFiles(HLib.path_cacheFolder_temp))
                try
                {
                    File.Delete(f);
                }
                catch { }
        }
        catch { }

        StartCoroutine(LoadRML());
    }

    void Update()
    {
        foreach (var channel in Mod.AllListeningChannels)
        {
            if (channel == NetworkChannel.Channel_Session || channel == NetworkChannel.Channel_Game)
                continue;
            var messages = RAPI.ListenForAllNetworkMessagesOnChannel((int)channel);
            foreach (var m in messages)
                foreach (var mod in ModManagerPage.activeModInstances)
                    if (mod.TryProcessMessage(m.steamid, channel, m.message))
                        break;
        }
    }


    static List<GameObject> addedObjects = new List<GameObject>();
    static List<BuildMenuItemGroup> verticalGroups = new List<BuildMenuItemGroup>();
    static List<BuildMenuItemGroup> herizontalGroups = new List<BuildMenuItemGroup>();
    internal static void InsertBuildMenuOptions()
    {
        if (addedObjects.Any(x => !x))
            RemoveOptions();
        else if (addedObjects.Count != 0)
            return;
        var helper = ComponentManager<CanvasHelper>.Value;
        if (!helper) return;
        var menu = helper.GetMenu(MenuType.BuildMenu);
        if (menu == null || menu.menuObjects.Count == 0) return;
        var addToMenu = new List<Mod.BuildMenuItem>();
        foreach (var mod in ModManagerPage.activeModInstances)
        {
            var menuItems = mod.GetBuildMenuItems();
            if (menuItems != null)
                addToMenu.AddRange(menuItems);
        }
        if (addToMenu.Count == 0) return;
        SortItems(addToMenu);
        foreach (BuildMenuItem_SelectMainCategory mainCategory in menu.menuObjects[0].GetComponentsInChildren<BuildMenuItem_SelectMainCategory>(true))
        {
            herizontalGroups.Insert(0, new BuildMenuItemGroup());
            foreach (Transform horizontalChild in Traverse.Create(mainCategory).Field("horizontalParent").GetValue<GameObject>().transform)
            {
                if (horizontalChild.name == "BrownBackground")
                {
                    herizontalGroups[0].background = horizontalChild.GetComponent<RectTransform>();
                    continue;
                }
                var blockOption = horizontalChild.GetComponentInChildren<BuildMenuItem_SelectBlock>(true);
                var subCategory = horizontalChild.GetComponentInChildren<BuildMenuItem_SelectSubCategory>(true);
                if (blockOption || subCategory)
                {
                    if (!addedObjects.Contains(horizontalChild.gameObject))
                    {
                        herizontalGroups[0].objects++;
                        herizontalGroups[0].baseObjects++;
                    }
                    if (subCategory)
                    {
                        var items = subCategory.GetComponentsInChildren<BuildMenuItem_SelectBlock>(true).Cast(x => Traverse.Create(x).Field("buildableItem").GetValue<Item_Base>());
                        if (items.Count == 0) continue;
                        foreach (Mod.BuildMenuItem item in addToMenu)
                            if (item.addHorizontally && items.Exists(x => x.UniqueIndex == item.itemToPlaceNextTo.UniqueIndex))
                            {
                                var newTransform = Instantiate(horizontalChild, horizontalChild.parent, false);
                                var newChild = newTransform.GetComponentInChildren<BuildMenuItem_SelectSubCategory>(true);
                                addedObjects.Add(newTransform.gameObject);
                                Traverse.Create(newChild).Field("buildableItem").SetValue(item.itemToInsert);
                                Traverse.Create(newChild).Method("RefreshIcon").GetValue();
                                herizontalGroups[0].objects++;
                                var parent = Traverse.Create(newChild).Field("verticalParent").GetValue<GameObject>().transform;
                                var flag = false;
                                var back = parent.Find("BrownBackground")?.GetComponent<RectTransform>();
                                var count = 0;
                                for (var j = parent.childCount - 1; j >= 0; j--)
                                    if (parent.GetChild(j).GetComponentInChildren<BuildMenuItem_SelectBlock>(true))
                                    {
                                        if (flag)
                                            DestroyImmediate(parent.GetChild(j).gameObject);
                                        else
                                        {
                                            flag = true;
                                            var s = parent.GetChild(j).GetComponentInChildren<BuildMenuItem_SelectBlock>(true);
                                            Traverse.Create(s).Field("buildableItem").SetValue(item.itemToInsert);
                                            Traverse.Create(s).Method("RefreshIcon").GetValue();
                                        }
                                        count++;
                                    }
                                if (back && count > 0)
                                    back.offsetMin += new Vector2(0, (back.offsetMax.y - back.offsetMin.y) / count * (count - 1));
                            }
                    }
                    else
                    {
                        var itemOption = Traverse.Create(blockOption).Field("buildableItem").GetValue<Item_Base>();
                        if (!itemOption) continue;
                        foreach (Mod.BuildMenuItem item in addToMenu)
                            if (item.itemToPlaceNextTo.UniqueIndex == itemOption.UniqueIndex && item.addHorizontally)
                            {
                                var newTransform = Instantiate(horizontalChild, horizontalChild.parent, false);
                                var newOption = newTransform.GetComponentInChildren<BuildMenuItem_SelectBlock>(true);
                                addedObjects.Add(newTransform.gameObject);
                                Traverse.Create(newOption).Field("buildableItem").SetValue(item.itemToInsert);
                                Traverse.Create(newOption).Method("RefreshIcon").GetValue();
                                herizontalGroups[0].objects++;
                            }
                    }
                }
            }
        }
        foreach (BuildMenuItem_SelectSubCategory subCategory in menu.menuObjects[0].GetComponentsInChildren<BuildMenuItem_SelectSubCategory>(true))
        {
            verticalGroups.Insert(0, new BuildMenuItemGroup());
            foreach (Transform verticalChild in Traverse.Create(subCategory).Field("verticalParent").GetValue<GameObject>().transform)
            {
                if (verticalChild.name == "BrownBackground")
                {
                    verticalGroups[0].background = verticalChild.GetComponent<RectTransform>();
                    continue;
                }
                var blockOption = verticalChild.GetComponentInChildren<BuildMenuItem_SelectBlock>(true);
                if (blockOption)
                {
                    if (!addedObjects.Contains(verticalChild.gameObject))
                    {
                        verticalGroups[0].objects++;
                        verticalGroups[0].baseObjects++;
                    }
                    var itemOption = Traverse.Create(blockOption).Field("buildableItem").GetValue<Item_Base>();
                    if (!itemOption) continue;
                    foreach (Mod.BuildMenuItem item in addToMenu)
                        if (item.itemToPlaceNextTo.UniqueIndex == itemOption.UniqueIndex && !item.addHorizontally)
                        {
                            var newTransform = Instantiate(verticalChild, verticalChild.parent, false);
                            var newChild = newTransform.GetComponentInChildren<BuildMenuItem_SelectBlock>(true);
                            addedObjects.Add(newTransform.gameObject);
                            Traverse.Create(newChild).Field("buildableItem").SetValue(item.itemToInsert);
                            Traverse.Create(newChild).Method("RefreshIcon").GetValue();
                            verticalGroups[0].objects++;
                        }
                }
            }
        }
        foreach (BuildMenuItemGroup group in verticalGroups)
        {
            if (!group.background || group.baseObjects == group.objects) continue;
            float heightDifference = (group.background.offsetMin.y - group.background.offsetMax.y) / group.baseObjects * (group.objects - group.baseObjects);
            group.background.offsetMin += new Vector2(0, heightDifference);
            group.difference = heightDifference;
        }
        foreach (BuildMenuItemGroup group in herizontalGroups)
        {
            if (!group.background || group.baseObjects == group.objects) continue;
            float heightDifference = (group.background.offsetMax.x - group.background.offsetMin.x) / group.baseObjects * (group.objects - group.baseObjects);
            group.background.offsetMax += new Vector2(heightDifference, 0);
            group.difference = heightDifference;
        }
    }
    internal static void RemoveOptions()
    {
        foreach (var obj in addedObjects)
            if (obj)
                DestroyImmediate(obj);
        addedObjects.Clear();
        foreach (BuildMenuItemGroup group in verticalGroups)
            if (group.background && group.baseObjects != group.objects)
                group.background.offsetMin -= new Vector2(0, group.difference);
        foreach (BuildMenuItemGroup group in herizontalGroups)
            if (group.background && group.baseObjects != group.objects)
                group.background.offsetMax -= new Vector2(group.difference, 0);
        verticalGroups.Clear();
        herizontalGroups.Clear();
    }

    internal static void SortItems(List<Mod.BuildMenuItem> list)
    {
        list.Sort(new Comparison<Mod.BuildMenuItem>(
            (x, y) =>
              (y.itemToPlaceNextTo.UniqueIndex == x.itemToInsert.UniqueIndex ? 1 : (y.itemToInsert.UniqueIndex == x.itemToPlaceNextTo.UniqueIndex ? -1 : 0))
            + (y.addHorizontally == x.addHorizontally ? 0 : y.addHorizontally ? 2 : -2)));
    }
    class BuildMenuItemGroup
    {
        public RectTransform background;
        public int objects = 0;
        public int baseObjects = 0;
        public float difference;
    }

    [Serializable]
    private class ServerVersionInfo
    {
        public string clientVersion = "";
    }

    IEnumerator LoadRML()
    {
        Directory.GetFiles(HLib.path_cacheFolder_temp).Where(s => s.ToLower().EndsWith(".dll")).ToList().ForEach((s) => File.Delete(s));

        ComponentManager<RML_Main>.Value = this;
        var request = AssetBundle.LoadFromFileAsync(Path.Combine(HLib.path_dataFolder, "rml.assets"));
        yield return request;
        HLib.bundle = request.assetBundle;

        ComponentManager<RSocket>.Value = gameObject.AddComponent<RSocket>();

        HLib.missingTexture = HLib.bundle.LoadAsset<Sprite>("missing").texture;
        gameObject.AddComponent<RConsole>();
        while (!GameObject.Find("RConsole")) { yield return new WaitForEndOfFrame(); }

        try
        {
            var harmony = new Harmony("hytekgames.raftmodloader");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        ComponentManager<RawSharp>.Value = gameObject.AddComponent<RawSharp>();
        ComponentManager<SocketIOComponent>.Value = gameObject.AddComponent<SocketIOComponent>();
        ComponentManager<UnityMainThreadDispatcher>.Value = gameObject.AddComponent<UnityMainThreadDispatcher>();

        gameObject.AddComponent<CustomLoadingScreen>();

        var MenuAssetLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLMainMenu");
        yield return MenuAssetLoadRequest;

        GameObject tempmenuobj = MenuAssetLoadRequest.asset as GameObject;
        GameObject mainmenu = Instantiate(tempmenuobj, gameObject.transform);
        ComponentManager<MainMenu>.Value = mainmenu.AddComponent<MainMenu>();

        var HNotifyLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLNotificationSystem");
        yield return HNotifyLoadRequest;

        GameObject hnotifytempobj = HNotifyLoadRequest.asset as GameObject;
        GameObject hnotify = Instantiate(hnotifytempobj, gameObject.transform);
        ComponentManager<HNotify>.Value = hnotify.AddComponent<HNotify>();
        gameObject.AddComponent<RChat>();

        gameObject.AddComponent<NetworkModManager>();
    }
}