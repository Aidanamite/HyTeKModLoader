﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMLLibrary;
using HarmonyLib;
using System.Reflection.Emit;
using Debug = UnityEngine.Debug;

namespace RaftModLoader
{
    class SaveHandler
    {
        const string saveName = "\\/RaftModLoader_CustomWorldSaveData\\/";
        public static RGD CreateLocal()
        {
            var saveParent = RAPI.CreateUninitializedObject<RGD_Game>();
            saveParent.t = byte.MaxValue;
            saveParent.name = saveName;
            saveParent.behaviours = new List<RGD>();
            foreach (var mod in ModManagerPage.activeModInstances)
                if (!string.IsNullOrEmpty(mod.slug))
                {
                    var modSave = mod.LocalSave;
                    if (modSave != null)
                    {
                        var writerObject = RAPI.CreateUninitializedObject<RGD_TextWriterObject>();
                        writerObject.text = mod.slug;
                        saveParent.behaviours.Add(writerObject);
                        saveParent.behaviours.Add(modSave);
                    }
                }
            return saveParent;
        }
        public static bool TryRestoreLocal(RGD data)
        {
            var saveParent = data as RGD_Game;
            if (saveParent?.name == saveName && saveParent.t == byte.MaxValue && saveParent.behaviours != null && saveParent.behaviours.Count % 2 == 0)
            {
                var modSaves = new Dictionary<string, RGD>();
                for (int i = 0; i < saveParent.behaviours.Count; i += 2)
                    if (saveParent.behaviours[i] is RGD_TextWriterObject)
                        modSaves[(saveParent.behaviours[i] as RGD_TextWriterObject).text] = saveParent.behaviours[i + 1];
                    else
                        return false;
                if (modSaves.Count > 0)
                    foreach (var mod in ModManagerPage.activeModInstances)
                        if (!string.IsNullOrEmpty(mod.slug) && modSaves.TryGetValue(mod.slug, out var modSave))
                        {
                            mod.LocalSave = modSave;
                            modSaves.Remove(mod.slug);
                            if (modSaves.Count == 0)
                                break;
                        }
                if (modSaves.Count > 0)
                    foreach (var mod in ModManagerPage.activeModInstances)
                        if (!string.IsNullOrEmpty(mod.oldSlug) && modSaves.TryGetValue(mod.oldSlug, out var modSave))
                        {
                            mod.LocalSave = modSave;
                            modSaves.Remove(mod.oldSlug);
                            if (modSaves.Count == 0)
                                break;
                        }
                return true;
            }
            return false;
        }

        public static List<Message> CreateRemote()
        {
            var messages = new List<Message>();
            var start = RAPI.CreateUninitializedObject<Message_WorldInfo>();
            start.worldGuid = saveName;
            start.Type = ~Messages.NOTHING;
            messages.Add(start);
            foreach (var mod in ModManagerPage.activeModInstances)
                if (!string.IsNullOrEmpty(mod.slug))
                {
                    var modSave = mod.RemoteSave;
                    if (modSave != null)
                    {
                        var infoObject = RAPI.CreateUninitializedObject<Message_WorldInfo>();
                        infoObject.Type = ~Messages.NOTHING;
                        infoObject.worldGuid = mod.slug;
                        messages.Add(infoObject);
                        modSave.Type = ~Messages.NOTHING;
                        messages.Add(modSave);
                    }
                }
            start.traderReputation = messages.Count - 1;
            return messages;
        }

        static List<Message> caught;
        static int waiting = 0;
        public static bool TryRestoreRemote(Message message)
        {
            if (waiting > 0)
            {
                waiting--;
                caught.Add(message);
                if (waiting == 0)
                {
                    var modSaves = new Dictionary<string, Message>();
                    for (int j = 0; j < caught.Count; j += 2)
                        if (caught[j] is Message_WorldInfo)
                            modSaves[(caught[j] as Message_WorldInfo).worldGuid] = caught[j + 1];
                    if (modSaves.Count > 0)
                        foreach (var l in ModManagerPage.activeModInstances)
                            if (modSaves.TryGetValue(l.slug, out var modSave))
                            {
                                l.RemoteSave = modSave;
                                modSaves.Remove(l.slug);
                                if (modSaves.Count == 0)
                                    break;
                            }
                }
                return true;
            }
            var start = message as Message_WorldInfo;
            if (start?.worldGuid == saveName && start.Type == ~Messages.NOTHING)
            {
                caught = new List<Message>();
                waiting = start.traderReputation;
                return true;
            }
            return false;
        }
    }

    [HarmonyPatch(typeof(SaveAndLoad), "RestoreRGDGame")]
    static class Patch_LoadGame
    {
        static void Prefix(RGD_Game game)
        {
            if (game?.behaviours != null)
                foreach (RGD saveObject in game.behaviours)
                    try
                    {
                        if (SaveHandler.TryRestoreLocal(saveObject))
                        {
                            game.behaviours.Remove(saveObject);
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
        }
    }

    [HarmonyPatch(typeof(SaveAndLoad), "CreateRGDGame")]
    static class Patch_SaveGame
    {
        static void Postfix(RGD_Game __result)
        {
            try
            {
                __result.behaviours?.Insert(0, SaveHandler.CreateLocal());
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }

    [HarmonyPatch(typeof(Raft_Network), "GetWorld")]
    static class Patch_SendWorld
    {
        static void Postfix(List<Message> __result)
        {
            try
            {
                __result.InsertRange(0, SaveHandler.CreateRemote());
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }

    [HarmonyPatch(typeof(NetworkUpdateManager), "Deserialize")]
    static class Patch_RecieveWorld
    {
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            var code = instructions.ToList();
            var ind = code.FindIndex(x => x.opcode == OpCodes.Stloc_2);
            code.InsertRange(ind, new[] {
                new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Patch_RecieveWorld), nameof(OnRecieve)))
            });
            return code;
        }
        static Message OnRecieve(Message msg)
        {
            try
            {
                if (SaveHandler.TryRestoreRemote(msg))
                    return null;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            return msg;
        }
    }
}